import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TestGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestGUI frame = new TestGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 668);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Aufgabe 1: Hintergundfarbe \u00E4ndern");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(22, 106, 331, 46);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Dieser Text soll ver\u00E4ndert werden");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_1.setBounds(103, 41, 286, 46);
		contentPane.add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Gr\u00FCn");
		btnNewButton.setBounds(154, 162, 111, 35);
		contentPane.add(btnNewButton);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRot.setBounds(22, 162, 111, 35);
		contentPane.add(btnRot);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.setBounds(278, 162, 111, 35);
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.setBounds(22, 207, 111, 35);
		contentPane.add(btnGelb);
		
		JButton btnRot_1_1 = new JButton("Standardfarbe");
		btnRot_1_1.setBounds(154, 207, 111, 35);
		contentPane.add(btnRot_1_1);
		
		JButton btnRot_1_1_1 = new JButton("Frabe w\u00E4hlen");
		btnRot_1_1_1.setBounds(278, 207, 111, 35);
		contentPane.add(btnRot_1_1_1);
	}
}
