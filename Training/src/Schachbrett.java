

import java.io.PrintWriter;

public class Schachbrett {
	
	public static void main(String[] args) {
		
		PrintWriter out = new PrintWriter(System.out, true);
		out.println();
		out.println("Aufgabe");
		out.println("Versuche diese beiden Ausgaben zu erzeugen:");
		out.println();
		out.println();
		printSchachbrett();
		out.println();
		printSchachbrettReverse();
		
	}
	
//Aufgabe 2.b.a	
	
	public static void printSchachbrett(PrintWriter out) {
		
		for(int number = 8; number > 0; number --) {
			
			for(char letter = 'A' ; letter < 'I' ; letter ++) {
				out.print(letter + "" + number+ " ");
			}
			out.println();
		}
	}
	
//Aufgabe 2.b.b	
	
	public static void printSchachbrettReverse(PrintWriter out) {
		
		for(int number = 1; number < 9; number ++) {
			
			for(char letter = 'H' ; letter >= 'A' ; letter --) {
				out.print(letter + "" + number+ " ");
			}
			out.println();
		}
	}

//Aufgabe 2.b.c
	
	public static void printSchachbrett() {
		
		PrintWriter p = new PrintWriter(System.out, true);
		printSchachbrett(p);
	}
	
//Aufgabe 2.b.c
	
		public static void printSchachbrettReverse() {
			
			PrintWriter p = new PrintWriter(System.out, true);
			printSchachbrettReverse(p);
		}
}
