import java.util.ArrayList;
import java.util.Scanner;
public class ArrayListUA {


 public static void main (String[] args) {
	 
	 ArrayList<Integer> list = new ArrayList<Integer>();
	 
	 for(int i =0; i<20;i++) {
		 list.add((int) ((Math.random()*(9)) + 1));
	 } 
	 System.out.println("Ausgabe Liste mit Zufallszalen zwischen 1-9:");
	 listeAusgeben(list);
	 int suchzahl=suchzahl();
	 ausgabeSuchzahl(suchzahl,list);
	 ausgabeIndicesSuchzahl(suchzahl,list);
	 removeSuchzahl(suchzahl,list);
	 einfuegenVon0(list);
	 
 }
 public static void listeAusgeben(ArrayList<Integer> list) {
	 for(int i=0; i<list.size();i++) {
			 System.out.printf("myList[%2d] : %d\n", i, list.get(i));
		 }
	 System.out.println();
 }
 public static int suchzahl() {
	 System.out.println("Gesuchte Zahl (1-9) eingeben: ");
	 Scanner sc = new Scanner(System.in);
	 int suchzahl = sc.nextInt();
	 sc.close();
	 return suchzahl;
	 }
 
 public static void ausgabeSuchzahl(int suchzahl, ArrayList<Integer> list) {
	 int counter = 0;
	 for(Integer i : list) {
		 if(i==suchzahl) {
			 counter+=1;
		 }
	 }
	 System.out.println("Gesuchte Zahl ist "+counter+" Mal vorhanden.");
	 System.out.println("\n");
 }
public static void ausgabeIndicesSuchzahl(int suchzahl, ArrayList<Integer> list) {
	System.out.println("Die gesuchte Zahl kommt an folgenden Indices in der Liste vor:");
	for(int i=0; i<list.size();i++) {
		 if(list.get(i)==suchzahl) {
			 System.out.printf("myList[%2d] : %d\n", i, list.get(i));
		 }
	 }
	System.out.println("\n");
 }
public static void removeSuchzahl(int suchzahl, ArrayList<Integer> list) {
		
		 for(int i=list.size()-1; i>=0;i--) {
			 if(list.get(i)==suchzahl) {
				 list.remove(i);
			 }
		 
		}
		 System.out.println("Liste nach entfernen von "+suchzahl+":");
		 listeAusgeben(list);
		 System.out.println("\n");
 }
public static void einfuegenVon0(ArrayList<Integer> list) {
	 for(int i=0; i<list.size();i++) {
		 if(list.get(i)==5) {
			 list.add(i+1,0);
		 }
	 }
	 System.out.println("Liste mit 0 hinter jeder 5:");
	 listeAusgeben(list);
}

 
}